Name : Muhammad Ridho Ananda  
NPM : 1706028682  
Class : Advanced Programming B  

## Links
1. Git Exercise : https://gitlab.com/mridho2828/advanced-programming-task
2. Javari Park Festival : https://gitlab.com/mridho2828/DDP2-Assignment/tree/master/assignment-4

## My Notes
1. Git Branches Exercise
- Reason to use git branches : when you are working collaborately and you want to make changes without affecting other developers' repository, e.g. when you are working on a new feature.
- Git branches on javari park festival : I create a new branch called "new-branch" and change line 95 from 10% to 0%

2. Git Revert Usage
- Scenario to use git revert : when you want to create a new commit according to a choosen previous commit (like undo but maintain the "deleted" commit)
- Usage on javari park festival : I change line 95 from 10% to 0%, commit it, and then make a revert commit before according to that committed change